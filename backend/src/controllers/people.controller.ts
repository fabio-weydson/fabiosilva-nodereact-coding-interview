import {
    JsonController,
    Get,
    HttpCode,
    NotFoundError,
    Param, QueryParams,
} from 'routing-controllers';
import { PeopleProcessing } from '../services/people_processing.service';
import {GetPeopleQueryInterface} from "../types/people.types";

const peopleProcessing = new PeopleProcessing();


@JsonController('/people', { transformResponse: false })
export default class PeopleController {
    @HttpCode(200)
    @Get('/all')
    getAllPeople(@QueryParams() query: GetPeopleQueryInterface) {
        const people = peopleProcessing.getAll(query);

        if (!people) {
            throw new NotFoundError('No people found');
        }

        return {
            data: people,
        };
    }

    @HttpCode(200)
    @Get('/:id')
    getPerson(@Param('id') id: number) {
        const person = peopleProcessing.getById(id);

        if (!person) {
            throw new NotFoundError('No person found');
        }

        return {
            data: person,
        };
    }

    //find people by name or title
    @HttpCode(200)
    @Get('/search')
    searchPeople(@QueryParams() query: GetPeopleQueryInterface) {
        const people = peopleProcessing.getAll(query);

        if (!people) {
            throw new NotFoundError('No people found');
        }

        return {
            data: people,
        };
    }
}
