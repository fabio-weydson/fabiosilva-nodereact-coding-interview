import people_data from '../data/people_data.json';
import {GetPeopleQueryInterface, PeopleInterface} from "../types/people.types";

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    getAll(query: GetPeopleQueryInterface) {
        const { page=0 ,limit=10, company, gender, title} = query;
        const searchResults = people_data.filter((p: PeopleInterface) => {
            return (
                (!title || (p.title && p.title.toLowerCase() === title.toLowerCase())) &&
                (!gender || (p.gender.toLowerCase() === gender.toLowerCase())) &&
                (!company || (p.company.toLowerCase() === company.toLowerCase()))
            )
        });

        const start = page>0 ? (page - 1) * limit : 0;
        const end = start>0 ? (page*limit) : limit;

        return searchResults.slice(start, end);
    }
}
