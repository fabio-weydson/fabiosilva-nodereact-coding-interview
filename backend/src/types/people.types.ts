export interface PeopleInterface {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    gender: string;
    title: string | null;
    company: string;
    avatar: string;
}
export interface GetPeopleQueryInterface {
    page: number,
    limit?: number,
    company?: string,
    gender?: string,
    title?: string
}

