import React, {FC, useState, useEffect, useMemo} from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";
import {Pagination} from "@mui/lab";
import SearchBarComponent from "../components/search/searchBar.component";


const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [filteredUsers, setFilteredUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [searchTerm, setSearchTerm] = useState<string>('');

  useEffect(() => {
    const fetchData = async () => {
      const result = await backendClient.getAllUsers({page:currentPage});
      if(result.data.length) {
        setUsers(result.data);
        setFilteredUsers(result.data);
        setLoading(false);
      }
    };

    fetchData();
  },[currentPage]);

  const handleSearch = (term: string) => {
    const filteredResults = users.slice().filter((p: IUserProps) => {
      return (
          (p.title && p.title.toLowerCase().includes(term.toLowerCase())) ||
          (p.gender.toLowerCase().includes(term.toLowerCase())) ||
          (p.company && p.company.toLowerCase().includes(term.toLowerCase()))
      )
    });
    setFilteredUsers(filteredResults);
  }

  useMemo(()=>{
    return handleSearch(searchTerm);
  }, [searchTerm])



  return (
    <div style={{ paddingTop: "30px" }}>
      <SearchBarComponent searchTerm={searchTerm} onSearch={(term:string)=>{setSearchTerm(term)}}/>
      <div style={{ display: "flex", justifyContent: "space-between", flexDirection:"column"}}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
            <>
              <div>
                {filteredUsers.length
                    ? filteredUsers.map((user) => {
                      return <UserCard key={user.id} {...user} />;
                    })
                    : null}
              </div>
              <hr/>
              <Pagination page={currentPage}
                          onChange={(e,value)=>{setCurrentPage(value)}}
                          count={filteredUsers.length}
              />
            </>
        )}
      </div>
    </div>
  );
};
