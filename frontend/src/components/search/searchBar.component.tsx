import React from "react";

interface SearchBarComponentProps  {
    searchTerm?: string;
    onSearch: (term: string) => void;
}

const SearchBarComponent = (props:SearchBarComponentProps) => {
    const {
        searchTerm,
        onSearch
    } = props;

    return (
        <div style={{display:"flex", padding:10, height: 40, backgroundColor:"#a9e2ff"}}>
        <input
            style={{lineHeight: "40px", padding: 10, border: "none", outline: "none", flex:1}}
            type="text"
            placeholder="Filter results by company, title or genre"
            value={searchTerm}
            onChange={(e)=>onSearch(e.target.value)}
            />
        </div>
    );
}
export default SearchBarComponent;
